
package Connect;

import java.sql.Connection;
import java.sql.DriverManager;
 
public class DBConnection {
 public static Connection createConnection()
 {
 Connection con = null;
 String url = "jdbc:mysql://localhost:3306/db"; 
 //String url = "jdbc:oracle:thin:@localhost:1521:xe"; 
// String username = "system";
// String password = "system"; 
	 String username = "root";
    String password = "1234"; 
 
 try 
 {
 try 
 {
 Class.forName("com.mysql.cj.jdbc.Driver"); //loading mysql driver 
 } 
 catch (ClassNotFoundException e)
 {
 e.printStackTrace();
 } 
 con = DriverManager.getConnection(url, username, password); //attempting to connect to MySQL database
 System.out.println("Printing connection object "+con);
 } 
 catch (Exception e) 
 {
 e.printStackTrace();
 }
 return con; 
 }
}